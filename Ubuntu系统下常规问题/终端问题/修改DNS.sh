#查看自己系统版本 
sudo echo "当前系统版本是"
lsb_release -c

# 1)修改DNS地址（这里用echo实现，如果对vim编辑器熟练，也可以用vim打开resolv.conf文件修改）
sudo echo "nameserver 223.5.5.5" | sudo tee /etc/resolv.conf > /dev/null
# 可换的DNS有:
# 114公共DNS: 114.114.114.114和114.114.115.115
# 阿里的DNS: 223.5.5.5和223.6.6.6、
# 腾讯的DNS: 119.29.29.29和182.254.116.116
# 百度的DNS: 180.76.76.76
# Google的DNS: 8.8.8.8和8.8.4.4

# 2)测试是否修改成功（用gedit查看也行）
cat /etc/resolv.conf

# 3)重新更新（也可以重新执行你当时安装出错的命令，比如：sudo apt install ros-humble-desktop）
sudo apt-get update --fix-missing
