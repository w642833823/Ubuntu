####!/bin/bash   ##指定解释器：由哪个程序来执行脚本内容；#!表示幻数

###注意:#!/bin/bash必须写在第一行,否则会被认为是注释
#!/bin/sh
#sudo chmod +x git.sh
#sudo chmod 777 ./git.sh
#sudo sudo ./git.sh
echo "\n"
echo -e "\n\033[1;35m记录第一次使用git \n \033[0m\n"
sudo apt-get autoremove openssh-client -y 
     
sudo apt-get install openssh-client openssh-sftp-server openssh-server ssh -y
     
sudo apt-get install ssh-askpass -y

echo -e "\033[1;33m 已安装完成.10秒关闭终端.....\033[0m \n"
for i in {10..1}
do 
	echo -e  "\e[1;34m\t\t$i秒\033[0m"
	
	sleep 1
done

exit 
