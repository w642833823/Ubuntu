配置个人的用户名称和电子邮件地址：
安装好git后，
git config --global user.name "你的名字"
git config --global user.email "你的邮箱"

7、C：C是comment的缩写
-C表示提供一个注释，用于识别这个密钥。 "邮件地址@youremail.com"：用于识别这个密钥的注释
引号里的内容为注释的内容，所有" "里面不一定填邮箱，可以输入任何内容。
总结：当你创建ssh的时候：-t 表示密钥的类型 ，-b表示密钥的长度，-C 用于识别这个密钥的注释 ，这个注释你可以输入任何内容，很多网站和软件用这个注释作为密钥的名字。

添加你本机的SSH key。注意换了电脑是要重新添加的
ssh-keygen -t rsa -C "your765544"

你需要把邮件地址换成你自己的邮件地址，然后一路回车，使用默认值即可。

如果一切顺利的话，可以在用户主目录里找到.ssh目录，里面有id_rsa和id_rsa.pub两个文件，这两个就是SSH Key的秘钥对，id_rsa是私钥，不能泄露出去，id_rsa.pub是公钥，可以放心地告诉任何人。

cat ~/.ssh/id_rsa.pub
(6)回到浏览器中，填写标题，粘贴公钥
3 克隆项目
(1)在浏览器中点击进入github首页，再进入项目仓库的页面
(2)复制git地址
git clone git@github.com:was833823/C.git
查看是否配置完成
git config -l

推送本地仓库到gitee.com
 进入工程目录在命令行中输入
     git init #ls -al 可见.git文件
     然后使用git add * 加入所有文件，
     最后git commit -m ‘你的注释’  
     git remote add origin git@gitee.com:danielliu2017/HelloWorld.git
     git push -u orgin master #完成第一次推送

     的远端仓库地址使用ssh，不要使用Https。
提示：更新被拒绝，因为远程仓库包含您本地尚不存在的提交。这通常是因为另外
提示：一个仓库已向该引用进行了推送。再次推送前，您可能需要先整合远程变更
提示：（如 'git pull ...'）。
提示：详见 'git push --help' 中的 'Note about fast-forwards' 小节。

原因可能是之前上传时创建的.git文件被删除或更改，或者其他人在github上提交过代码．

解决方案如下：１．强行上传　　 git push -u origin +master

　　　　　　　2． 尽量先同步github上的代码到本地，在上面更改之后再上传
