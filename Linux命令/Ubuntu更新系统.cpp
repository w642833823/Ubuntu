

更新ubuntu22.04LTS有两个步骤，1.更新APT储存库sudo apt update 2.系统更新sudo apt upgrade -y

步骤1：更新APT储存库

sudo apt update

步骤2：更新软件包（系统更新）

sudo apt upgrade -y

通过命令行升级

通过下方命令可升级 Ubuntu 的桌面版：

sudo do-release-upgrade -m desktop

通过 UI 升级

使用下方命令打开软件更新管理器界面通过 GUI 更新软件，并检查 Ubuntu 系统的更新：

sudo update-manager -c -d

升级完成检查

输入 uname -a 和 lsb_release -a 即可发现，系统已经完成升级。
