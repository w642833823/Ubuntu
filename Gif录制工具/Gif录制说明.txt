在ubuntu软件中心搜索byzanz或者通过apt-get下载

sudo apt-get install byzanz

下载完成后打开命令行输入byzanz-record --help
这里写图片描述
其中我们重点关注几个参数
* -d 动画录制的时间,默认录制10秒
* -e 动画开始延迟
* -x 录制区域的起始X坐标
* -y 录制区域的起始Y坐标
* -w 录制区域的宽度
* -y 录制区域的高度

那么怎么知道我要录制区域的坐标和高度呢,其实系统自带这个命令工具.
输入xwininfo会提示你选择一个窗口,点击之后会返回这个窗口的详细信息
这里写图片描述

    Absolute upper-left X: 开始的X坐标,绝对值
    Absolute upper-left Y: 同上
    Width: 窗口宽度
    Height : 窗口高度

有了这四个值我们就可以开始录制了.

byzanz-record -x 482 -y 53 -w 452 -h 715 ~/x.gif
————————————————
版权声明：本文为CSDN博主「p1aywind」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/zheng5229875/article/details/47358963
