#include "list_version.h"
#include "ui_list_version.h"

List_Version::List_Version(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::List_Version)
{
    ui->setupUi(this);
}

List_Version::~List_Version()
{
    delete ui;
}

void List_Version::on_btn_version_clicked()
{
    unsigned codecVer = avcodec_version();
    int ver_major,ver_minor,ver_micro;
    ver_major = (codecVer>>16)&0xff;
    ver_minor = (codecVer>>8)&0xff;
    ver_micro = (codecVer)&0xff;
    QString avdevice_ver=QString("avdevice_version:%1\n").arg(avdevice_version());
    QString ffmpeg=QString("\nFFmpeg版本：%1\n").arg(FFMPEG_VERSION);
    QString pei=QString("FFmpeg 配置信息 is: %1 \n").arg(avcodec_configuration());
    QString avcodec_version=QString(" avcodec version is: %1=%2.%3.%4.\n").arg(codecVer).arg(ver_major).arg(ver_minor).arg(ver_micro);
   ui->textBrowser->append(ffmpeg);
    ui->textBrowser->append(avdevice_ver);
    ui->textBrowser->append(pei);
    ui->textBrowser->append(avcodec_version);
}
