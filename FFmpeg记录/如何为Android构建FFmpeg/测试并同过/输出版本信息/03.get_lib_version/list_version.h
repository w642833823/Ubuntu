#ifndef LIST_VERSION_H
#define LIST_VERSION_H

#include <QWidget>
extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
    #include <libavdevice/avdevice.h>
    #include <libavformat/version.h>
    #include <libavutil/time.h>
    #include <libavutil/mathematics.h>
//版本
   #include <libavutil/ffversion.h>

}
namespace Ui {
class List_Version;
}

class List_Version : public QWidget
{
    Q_OBJECT

public:
    explicit List_Version(QWidget *parent = nullptr);
    ~List_Version();

private slots:
    void on_btn_version_clicked();

private:
    Ui::List_Version *ui;
};

#endif // LIST_VERSION_H
