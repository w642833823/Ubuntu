TEMPLATE = app
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT       += androidextras
# 包含FFmpeg库文件位置

# linux平台
unix{

INCLUDEPATH += $$PWD/ffmpeg/include
LIBS += $$PWD/ffmpeg/lib/libavformat.so  \
        $$PWD/ffmpeg/lib/libavcodec.so    \
        $$PWD/ffmpeg/lib/libavdevice.so   \
        $$PWD/ffmpeg/lib/libavfilter.so   \
        $$PWD/ffmpeg/lib/libavutil.so     \
        $$PWD/ffmpeg/lib/libswresample.so \
        $$PWD/ffmpeg/lib/libswscale.so\
        $$PWD/ffmpeg/lib/libpostproc.so\

}
FORMS += \
    list_version.ui

HEADERS += \
    list_version.h

SOURCES += \
        list_version.cpp \
        main.cpp

ANDROID_EXTRA_LIBS = $$PWD/ffmpeg/lib/libavcodec.so $$PWD/ffmpeg/lib/libavdevice.so $$PWD/ffmpeg/lib/libavfilter.so $$PWD/ffmpeg/lib/libavformat.so $$PWD/ffmpeg/lib/libavutil.so $$PWD/ffmpeg/lib/libpostproc.so $$PWD/ffmpeg/lib/libswresample.so $$PWD/ffmpeg/lib/libswscale.so

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

