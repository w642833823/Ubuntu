#include <stdio.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

int main() {
    // 注册所有的FFmpeg组件
    av_register_all();

    // 打开输入文件
    AVFormatContext *formatContext = NULL;
    if (avformat_open_input(&formatContext, "input.mp4", NULL, NULL) != 0) {
        printf("无法打开输入文件\n");
        return -1;
    }

    // 获取流信息
    if (avformat_find_stream_info(formatContext, NULL) < 0) {
        printf("无法获取流信息\n");
        return -1;
    }

    // 寻找视频流
    int videoStreamIndex = -1;
    for (int i = 0; i < formatContext->nb_streams; i++) {
        if (formatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            videoStreamIndex = i;
            break;
        }
    }

    if (videoStreamIndex == -1) {
        printf("无法找到视频流\n");
        return -1;
    }

    // 获取视频解码器
    AVCodecParameters *codecParameters = formatContext->streams[videoStreamIndex]->codecpar;
    AVCodec *codec = avcodec_find_decoder(codecParameters->codec_id);
    if (codec == NULL) {
        printf("无法找到解码器\n");
        return -1;
    }

    // 创建解码器上下文
    AVCodecContext *codecContext = avcodec_alloc_context3(codec);
    if (avcodec_parameters_to_context(codecContext, codecParameters) < 0) {
        printf("无法创建解码器上下文\n");
        return -1;
    }

    // 打开解码器
    if (avcodec_open2(codecContext, codec, NULL) < 0) {
        printf("无法打开解码器\n");
        return -1;
    }

    // 读取视频帧
    AVPacket packet;
    while (av_read_frame(formatContext, &packet) >= 0) {
        if (packet.stream_index == videoStreamIndex) {
            // 解码视频帧
            AVFrame *frame = av_frame_alloc();
            int response = avcodec_send_packet(codecContext, &packet);
            if (response < 0) {
                printf("解码出错\n");
                break;
            }

            response = avcodec_receive_frame(codecContext, frame);
            if (response == AVERROR(EAGAIN) || response == AVERROR_EOF) {
                av_frame_free(&frame);
                continue;
            } else if (response < 0) {
                printf("解码出错\n");
                break;
            }

            // 处理视频帧
            printf("解码视频帧，宽度：%d，高度：%d\n", frame->width, frame->height);

            av_frame_free(&frame);
        }

        av_packet_unref(&packet);
    }

    // 清理资源
    avcodec_free_context(&codecContext);
    avformat_close_input(&formatContext);

    return 0;
}

