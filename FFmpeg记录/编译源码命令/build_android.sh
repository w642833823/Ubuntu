#!/bin/bash

export NDK=/home/w642833823/Android/android-ndk-r20
export TOOLCHAIN=$NDK/toolchains/llvm/prebuilt/linux-x86_64
export SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
export API=29
# 配置选项    disable 表示关闭    enable 表示开启
function build_android {
    echo "Compiling FFmpeg for $CPU"
    ./configure \
        --prefix=$PREFIX \
        --libdir=$LIB_DIR \
        --enable-neon \
        --enable-hwaccels \
        --enable-gpl \
        --enable-postproc \
        --enable-shared \
        --enable-jni \
        --enable-mediacodec \
        --disable-decoders \
        --enable-decoder=h264_mediacodec \
        --enable-decoder=h264 \
        --enable-decoder=mpeg4 \
        --enable-decoder=aac \
        --enable-decoder=aac_latm \
        --enable-decoder=mjpeg \
        --enable-decoder=png \
        --enable-decoder=mpeg4_mediacodec \
        --disable-encoders \
        --enable-encoder=h264_v4l2m2m \
        --disable-demuxers \
        --enable-demuxer=rtsp \
        --enable-demuxer=rtp \
        --enable-demuxer=flv \
        --enable-demuxer=h264 \
        --disable-muxers \
        --enable-muxer=rtsp \
        --enable-muxer=rtp \
        --enable-muxer=flv \
        --enable-muxer=h264 \
        --disable-parsers \
        --enable-parser=mpeg4video \
        --enable-parser=aac \
        --enable-parser=h264 \
        --enable-parser=vp9 \
        --disable-protocols \
        --enable-protocol=rtmp \
        --enable-protocol=rtp \
        --enable-protocol=tcp \
        --enable-protocol=udp \
        --disable-bsfs \
        --disable-indevs \
        --enable-indev=v4l2 \
        --disable-outdevs \
        --disable-filters \
        --disable-postproc \
        --disable-static \
        --disable-doc \
        --disable-ffmpeg \
        --disable-ffplay \
        --disable-ffprobe \
        --disable-avdevice \
        --disable-symver \
        --target-os=android \
        --enable-small \
        --disable-swresample \
        --disable-postproc \
        --disable-programs \
        --disable-runtime-cpudetect \
        --disable-htmlpages \
        --disable-manpages \
        --disable-podpages \
        --disable-txtpages \
        --disable-stripping \
        --arch=$ARCH \
        --cpu=$CPU \
        --cc=$CC \
        --cxx=$CXX \
        --enable-cross-compile \
        --sysroot=$SYSROOT \
        --extra-cflags="-Os -fpic $OPTIMIZE_CFLAGS" \
        --extra-ldflags="$ADDI_LDFLAGS" \
        --disable-x86asm \
        --disable-asm \
        $COMMON_FF_CFG_FLAGS
    make clean
    make -j8
    make install
    echo "The Compilation of FFmpeg for Android[$CPU] is completed.\n"
}

# # armv8-a
# OUTPUT_FOLDER="arm64-v8a"
# ARCH=arm64
# CPU="armv8-a"
# TOOL_CPU_NAME=aarch64
# TOOL_PREFIX=${TOOLCHAIN}/bin/${TOOL_CPU_NAME}-linux-android
# CC="${TOOL_PREFIX}${API}-clang"
# CXX="${TOOL_PREFIX}${API}-clang++"
# PREFIX="${PWD}/output/${OUTPUT_FOLDER}"
# LIB_DIR="${PWD}/output/libs/${OUTPUT_FOLDER}"
# OPTIMIZE_CFLAGS="-march=${CPU}"
# build_android

# armv7-a
OUTPUT_FOLDER="armeabi-v7a"
ARCH="arm"
CPU="armv7-a"
TOOL_CPU_NAME=armv7a
TOOL_PREFIX=${TOOLCHAIN}/bin/${TOOL_CPU_NAME}-linux-android
CC="$TOOLCHAIN/bin/armv7a-linux-androideabi${API}-clang"
CXX="$TOOLCHAIN/bin/armv7a-linux-androideabi${API}-clang++"
PREFIX="${PWD}/output/${OUTPUT_FOLDER}"
LIB_DIR="${PWD}/output/libs/${OUTPUT_FOLDER}"
OPTIMIZE_CFLAGS="-march=${CPU}"
build_android

# # x86
# OUTPUT_FOLDER="x86"
# ARCH="x86"
# CPU="x86"
# TOOL_CPU_NAME="i686"
# TOOL_PREFIX=${TOOLCHAIN}/bin/${TOOL_CPU_NAME}-linux-android
# CC="${TOOL_PREFIX}${API}-clang"
# CXX="${TOOL_PREFIX}${API}-clang++"
# PREFIX="${PWD}/output/${OUTPUT_FOLDER}"
# LIB_DIR="${PWD}/output/libs/${OUTPUT_FOLDER}"
# OPTIMIZE_CFLAGS="-march=i686 -mtune=intel -mssse3 -mfpmath=sse -m32"
# build_android

# #  x86_64
# OUTPUT_FOLDER="x86_64"
# ARCH="x86_64"
# CPU="x86-64"
# TOOL_CPU_NAME="x86_64"
# TOOL_PREFIX=${TOOLCHAIN}/bin/${TOOL_CPU_NAME}-linux-android
# CC="${TOOL_PREFIX}${API}-clang"
# CXX="${TOOL_PREFIX}${API}-clang++"
# PREFIX="${PWD}/output/${OUTPUT_FOLDER}"
# LIB_DIR="${PWD}/output/libs/${OUTPUT_FOLDER}"
# OPTIMIZE_CFLAGS="-march=${CPU}"
# build_android
