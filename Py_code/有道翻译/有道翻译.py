import hashlib
import json
import random
import time

import requests

if __name__=="__main__":
    while True:
        e=input("请输入要翻译的关键字：")
        if e=="q":
            break
        if len(e)and e !="":
            #产生0-9的随机数并把它变成字符串
            rsum=str(random.randint(0,10))
            #产生时间辍
            #time.time()-->产生距离1978年1月1日的秒数
            r=str(int(time.time()*1000))
            lts=r
            salt=r+rsum
            s="fanyideskweb"+e+r+rsum+"Ygy_4c=r#e#4EX^NUGUc5"
            sign=hashlib.md5(s.encode()).hexdigest()
            data = {
                "i":e,
                "from":"AUTO",
                "to":"AUTO",
                "smartresult":"dict",
                "client":"fanyideskweb",
                "salt":salt,
                "sig":sign,
                "lts":lts,
                "bv":"1de9313c44872e4c200c577f99d4c09e",
                "doctype":"json",
                "version":"2.1",
                "keyfrom":"fanyi.web",
                "action":"FY_BY_REALTlME"

            }
            url="https://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule"
            header = {
                "User-Agent":"Mozilla/5.0 (X11; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0",
                "Referer":"https://fanyi.youdao.com/",
                "Cookie":"YOUDAO_MOBILE_ACCESS_TYPE=1; OUTFOX_SEARCH_USER_ID=1826706619@10.110.96.158; ___rl__test__cookies=1655886746101; OUTFOX_SEARCH_USER_ID_NCOO=1195227539.2836363; fanyi-ad-id=306808; fanyi-ad-closed=1"
            }
            res_data=requests.post(url,headers=header,data=data)
            print(res_data.text)
            res_dic=json.loads(res_data.text)
            word = res_dic.get("translateResult","")[0][0].get("tgt","")
            print(word)

