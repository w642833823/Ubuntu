
# coding: utf-8

import re
import subprocess
import calendar
from datetime import datetime


def parse_month(mon_num):
    return calendar.month_abbr[mon_num]

def get_re_match_result(pattern, string):
    match = re.search(pattern, string)
    return match.group(1)

def parse_time(date_str):

    newdata_str=re.search(r'[\u4e00-\u9fa5]',date_str)
    if newdata_str:
        date_list=date_str.split(" ")
        chinmon=date_list[0]
        num_mon=re.sub(r'[\u4e00-\u9fa5]',"", chinmon)
        mon = parse_month(int(num_mon))
        date_list[0]=mon
        date_str = " ".join(date_list)

    return datetime.strptime(date_str, "%b %d %H:%M:%S %Y GMT")

def format_time(date_time):
    return datetime.strftime(date_time, "%Y-%m-%d %H:%M:%S")


def get_cert_info(domain):
    """获取证书信息"""
    cmd = f"curl -Ivs https://{domain} --connect-timeout 10"

    exitcode, output = subprocess.getstatusoutput(cmd)
    # print("output",output)
    # 正则匹配
    start_date = get_re_match_result('start date: (.*)', output)
    expire_date = get_re_match_result('expire date: (.*)', output)
    issuer = get_re_match_result('issuer: (.*)', output)
    Host = get_re_match_result('Host: (.*)', output)


    # 解析匹配结果
    start_date = parse_time(start_date)
    expire_date = parse_time(expire_date)

    return {
        'start_date': start_date,
        'expire_date': expire_date,
        'expire_day': (expire_date - datetime.now()).days,
        'issuer': issuer,
        'Host': Host
    }


def exe_func(domain_list):
    for dom in domain_list:
        domain_expire_info = get_cert_info(dom)
        print("域名： ", domain_expire_info.get("Host", "未获取到"))
        print("证书生效日期： ",domain_expire_info.get("start_date","未获取到"))
        print("证书到期时间： ", domain_expire_info.get("expire_date", "未获取到"))
        print("证书发行商： ", domain_expire_info.get("issuer", "未获取到"))
        print("证书还有 " + str(domain_expire_info.get("expire_day", "未获取到")) + " 天到期")
        print("*" * 30)


if __name__ == "__main__":
    domain_list=["www.baidu.com","www.163.com"]
    exe_func(domain_list)