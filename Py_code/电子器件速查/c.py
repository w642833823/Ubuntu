# -*- coding: utf-8 -*-
import sys,os
import requests, json
from bs4 import BeautifulSoup as bSoup

import threading

class Device:
    def __init__(self, num, info):
        print("Number : ", num)
        self.device_info = info
        self.url = "https://item.szlcsc.com/" + str(num) + ".html"

    def get_device(self):
        ret = requests.get(self.url)
        content = ret.content.decode("utf-8")
        
        soup = bSoup(content, 'lxml')

        tables = soup.find('table', class_ = "param-body")
        link = soup.find('a', id = "lookAllPdf")
        
        self.get_device_content(tables)
        self.get_device_pdf(link)


    def get_device_content(self, tables):
        temp_list = []
        
        for i in tables.find_all('tr'):
            for s in i.find_all('td'):
                t = s.get_text()
                if t != "\n\n":
                    temp_list.append(t)
                else:
                    self.device_info.update({temp_list[0] : temp_list[1]})
                    temp_list.clear()


    def get_device_pdf(self, link):
        if link:
            l = link['href']
            l = l.split('javascript:fileModule.downloadFileNoRemark(\'')[1]
            l = l.split('%3F')[0]
            
            self.device_info.update({'pdf' : l})
        else:
            self.device_info.update({'pdf' : "None"})

    def put_device_info(self):
        for key in self.device_info.keys():
            print("{:<20}{:<40}".format(key, self.device_info[key]))


class Types:
    def __init__(self, url):
        self.url = url

    def get_type(self):
        ret = requests.get(self.url)
        content = ret.content.decode("utf-8")

        #print(content)
        
        soup = bSoup(content, 'lxml')

    def get_index_note(self, span):
        print(span)



class Tables_list:
    def __init__(self):
        self.url = 'https://list.szlcsc.com/products/list'
        self.data = {
            "catalogNodeId" : "",
            "pageNumber" : "",
            "querySortBySign" : "0",
            "showOutSockProduct" : "1",
            "showDiscountProduct" : "1",
            "queryBeginPrice" : "",
            "queryEndPrice" : "",
            "queryProductArrange" : "",
            "queryProductGradePlateId" : "",
            "queryParameterValue" : "",
            "queryProductStandard" : "",
            "queryReferenceEncap" : "",
            "queryProductLabel" : "",
            "lastParamName" : "",
            "baseParameterCondition" : "",
            "parameterCondition" : ""
            }

        self.info = {
            "productName" : "",         #产品名
            "lightBrandName" : "",      #品牌
            "productModel" : "",        #厂家型号
            "productCode" : "",         #商品编号
            "encapsulationModel" : "",  #封装
            "breviaryImageUrl" : ""     #产品小图片
            }

        self.list_info = ["productName", "lightBrandName", "productModel", "productCode", "encapsulationModel", "breviaryImageUrl"]

    def get(self, node, page):
        self.data["catalogNodeId"] = str(node)
        self.data["pageNumber"] = str(page)
        
        ret = requests.post(self.url, data = self.data)

        for i in ret.json().get('productRecordList'):
            temp_info = self.info
            
            for n in self.list_info:
                temp_info[n] = i[n]
                pass

            print(temp_info)
            self.get_device(i['productId'], temp_info)
            print()

    def get_device(self, num, info):
        device = Device(num, info)
        device.get_device()
        device.put_device_info()


def main():
    #ret = requests.get("https://www.szlcsc.com/")

    #content = ret.content.decode("utf-8")

    #soup = bSoup(content, "lxml")

    #s = soup.find_all("li", class_ = "ass-list")

    #d = Device("47454")
    #d.get_device()
    #d.put_device_info()

    t = Tables_list()
    t.get(439,1)

if __name__ == "__main__":
    main()


