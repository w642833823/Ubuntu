import json
from tqdm import tqdm
import requests

from datetime import datetime

def get_filename():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")

if __name__=="__main__":
    page_sizes=int(input("请输入你要爬取的视频的个数："))
    
    header={
        "User-Agent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0"
        }
    res_data=requests.get(f"https://www.ku6.com/video/feed?"
                          f"pageNo=0&pageSize={page_sizes}&subjectId=70",
                          headers=header)
    res_dic=json.loads(res_data.text)
    for item in res_dic["data"]:
        video_url=item["playUrl"]
        video_file_name=item["title"]
        #打印视频地址
        #print(video_url)
        video_data=requests.get(video_url,headers=header)
        if "content-length" in video_data.headers:
            #把字符串变成int
            video_size=int(video_data.headers["content-length"])
        
            filename=video_file_name
        #注意存放地址是否正确
            with  open(f"/home/w642833823/video/{filename}.mp4","wb") as f, tqdm(
                 desc=filename,
                 total=video_size
            ) as bar:
            
                 for chunk in video_data.iter_content(chunk_size=1024*1024):
                
                     if chunk:
                        f.write(chunk)
                        bar.update(len(chunk))
