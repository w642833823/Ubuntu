import os
from turtle import width
import requests
import re
import json

from lxml import etree
headers ={
    'Referer':'https://www.bilibili.com/',
    'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0'
    }
#获取视频页的网页源代码
url = 'https://www.bilibili.com/video/BV1AA4y1D7h2/?vd_source=5fb207316e3b77a15884783d3c143acf'
r = requests.get(url,headers=headers)
#print(r.text) 
#提取视频和音频的播放地址
info =re.findall('window.__playinfo__=(.*?)</script>',
 r.text)[0]
video_url = json.loads(info)["data"]["dash"]["video"][0]['baseUrl']
audio_url = json.loads(info)["data"]["dash"]["audio"][0]['baseUrl']
html = etree.HTML(r.text)
filename = html.xpath('//h1/text()')[0]
#print(filename)
print('开始下载了......')
#分别下载视频和音频
video_content = requests.get(video_url,headers=headers).content
audio_content = requests.get(audio_url,headers=headers).content
with open(f'/home/w642833823/video/{filename}.mp4','wb') as f:
    f.write(video_content)
    print('以下载视频部分')

with open(f'/home/w642833823/video/{filename}.mp3','wb') as f:
    f.write(audio_content)
    print('已下载音频部分')
#合并视频和音频
#ffmpeg
print('、开始合并。。。。。')
cmd = fr"ffmpeg -i /home/w642833823/video/{filename}.mp4 -i /home/w642833823/video/{filename}.mp3 -c:v copy -c:a aac -strict experimental /home/w642833823/video/output-{filename}.mp4 -loglevel quiet "
os.system(cmd)
print('合并完成')
os.remove(f'/home/w642833823/video/{filename}.mp4')
os.remove(f'/home/w642833823/video/{filename}.mp3')
print('已删除多余的文件')