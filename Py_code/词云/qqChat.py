import matplotlib.pyplot as plt
from wordcloud import WordCloud
import jieba
from wordcloud import WordCloud, ImageColorGenerator, STOPWORDS
import os
import numpy as np
import PIL.Image as Image
 
newtext = []
#打开聊天记录文件
for word in open('/home/w642833823/bbi/chat.txt', 'r',encoding='utf-8'):
    tmp = word[0:4]
    #过滤掉聊天记录的时间和qq名称
    if (tmp == "2021"):
        continue
    tmp = word[0:2]
    #print(tmp)
    newtext.append(word)
 
#将过滤后的文本重新写入文件并保存
with open('/home/w642833823/bbi/chat_final.txt', 'w', encoding='utf-8') as f:
    for i in newtext:
        f.write(i)
#打开新生成的聊天记录文件
text = open('/home/w642833823/bbi/chat_final.txt', 'r',encoding = 'utf-8').read()
word_jieba = jieba.cut(text, cut_all=True)
word_split = " ".join(word_jieba)
#去掉一些意义不大的字
stop_words = set(STOPWORDS)
stop_words.add("的")
stop_words.add("我")
stop_words.add("你")
stop_words.add("是")
stop_words.add("就")
#根据心形图片生成配色方案，这里的路径要配置正确
alice_coloring = np.array(Image.open(os.path.join('/home/w642833823/bbi/','chat.jpg')))
my_wordcloud = WordCloud(scale=16,background_color="white", max_words=800, stopwords=stop_words, mask=alice_coloring,max_font_size=80, random_state=42,font_path='/home/w642833823/bbi/simhei.ttf')\
    .generate(word_split)
image_colors = ImageColorGenerator(alice_coloring)
plt.imshow(my_wordcloud.recolor(color_func=image_colors))
plt.imshow(my_wordcloud)
plt.axis("off")
plt.show()
