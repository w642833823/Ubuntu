#coding=utf-8
import zipfile  
import optparse  #optparse用于设置可选参数，并进行参数的解释
from threading import Thread  #从threading库导入Thread类 主要功能是设置多线程

def extractFile(zFile,password):  
    try:  
        zFile.extractall(pwd=password)   #python3中pwd需要的是byte
        print ('[+] Fonud Password : ' + password + '\n')  
    except:  
        pass  

def main():  

    parser = optparse.OptionParser("[*] Usage: ./unzip.py -f <zipfile> -d <dictionary>") #设置参数解释
    parser.add_option('-f',dest='zname',type='string',help='specify zip file')  #设置参数 可输入读取目标文件
    parser.add_option('-d',dest='dname',type='string',help='specify dictionary file')  #设置字典参数
    (options,args) = parser.parse_args()  #options可以理解为sys.argv[1:] 比如(-f xxx.zip -d x.txt)

    if (options.zname == None) | (options.dname == None):  #确认参数是否存在
        print (parser.usage)  #输出我们前面设置的解释
        exit(0)  

    zFile = zipfile.ZipFile(options.zname)  #确定破解目标对象
    passFile = open(options.dname)         #打开我们需要的字典
    for line in passFile:   #遍历字典内容
        line = line.strip('\n')  
        t = Thread(target=extractFile,args=(zFile,line))  #设置多线程 target可理解为等于一个方法（自定义函数），args可理解为我们向函数中传的参数 这里就是像extractall()中传入破解ZIP文件和遍历的字典密码
        t.start()  #开启线程

if __name__ == '__main__':   #如果在当前脚本执行 那么为真
    main()  