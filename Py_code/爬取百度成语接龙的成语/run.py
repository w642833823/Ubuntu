# -*- coding: utf-8 -*-
# @Author   : LiMengDi
# @FILE     : run.py.py
# @Time     : 2019/11/28 14:43
# @Software : PyCharm
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from chengyu import Ui_MainWindow
from chengyujielong import get_list,get_first_word,jiqijie

class mwindow(QWidget, Ui_MainWindow):
    def __init__(self):
        super(mwindow, self).__init__()
        self.setupUi(self)
        self.url_list = get_list()
        self.randoms = get_first_word(self.url_list)

    def main_s(self):
        self.randoms_first(self.randoms)
        self.pushButton.clicked.connect(self.get_zhurenjie)

    def randoms_first(self,randoms):
        self.label_4_first_random.setText(randoms)

    def get_zhurenjie(self):
        text=self.lineEdit_2_zhurenjie.text()
        self.lineEdit_2_zhurenjie.clear()
        if text=='退出':
            app.quit()
        if text == '我认输':
            app.quit()
        if text not in self.url_list:
            self.label_3_qingkuang.setText('耍赖，这不是个成语或俗语,重新说')
        elif text[0] != self.randoms[-1]:
            self.label_3_qingkuang.clear()
            self.label_3_qingkuang.setText('耍赖,你没有接最后一个字，最后一个字是：{}'.format(str(self.randoms[-1])))
        else:
            self.label_3_qingkuang.clear()
            try:
                jiqi = jiqijie(self.url_list, text)
                self.randoms=jiqi
                self.label_4_first_random.clear()
                self.label_3_diannaojie.setText('俺接：{}'.format(str(jiqi)))
            except:
                self.label_3_qingkuang.setText('俺接不出来，你赢了！')

if __name__ == '__main__':

    app = QApplication(sys.argv)
    w = mwindow()
    w.show()
    w.main_s()
    sys.exit(app.exec_())