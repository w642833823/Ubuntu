# This Python file uses the following encoding: utf-8
from enum import Enum, auto
import copy

堆栈长度=4
class 颜色(Enum):
    红 = auto()
    橙 = auto()
    黄 = auto()
    绿 = auto()
    青 = auto()
    蓝 = auto()
    紫 = auto()
    浅绿 = auto()
    深绿 = auto()
    黑 = auto()
    灰 = auto()
    粉 = auto()
    灰绿 = auto()

## 152
开始列表 =[
        [],
        [],
        [颜色.蓝,颜色.深绿,颜色.灰,颜色.灰],
        [颜色.黄,颜色.橙,颜色.黑,颜色.绿],
        [颜色.粉,颜色.绿,颜色.红,颜色.黄  ],
        [颜色.橙,颜色.浅绿,颜色.灰,颜色.黄],
        [颜色.灰绿,颜色.灰,颜色.红,颜色.紫],
        [颜色.浅绿,颜色.紫,颜色.绿,颜色.粉],

        [颜色.黄,颜色.深绿,颜色.橙,颜色.深绿],
        [颜色.绿,颜色.紫,颜色.浅绿,颜色.红],
        [颜色.浅绿,颜色.紫,颜色.黑,颜色.黑],
        [颜色.深绿,颜色.粉,颜色.灰绿,颜色.红 ],
        [颜色.蓝,颜色.灰绿,颜色.蓝,颜色.蓝],
        [颜色.灰绿,颜色.黑,颜色.橙,颜色.粉]        
        ]

## 266
开始列表 =[
        [],
        [],
        [颜色.紫,颜色.红,颜色.橙,颜色.黑],
        [颜色.灰,颜色.粉,颜色.粉,颜色.粉],
        [颜色.绿,颜色.红,颜色.红,颜色.深绿],
        [颜色.深绿,颜色.灰,颜色.灰绿,颜色.紫],
        [颜色.黄,颜色.紫,颜色.深绿,颜色.黑],
        [颜色.紫,颜色.浅绿,颜色.灰绿,颜色.蓝],

        [颜色.黄,颜色.浅绿,颜色.粉,颜色.绿],
        [颜色.蓝,颜色.绿,颜色.黑,颜色.橙],
        [颜色.深绿,颜色.灰绿,颜色.黄,颜色.绿],
        [颜色.黄,颜色.浅绿,颜色.蓝,颜色.灰绿 ],
        [颜色.灰,颜色.灰,颜色.黑,颜色.蓝],
        [颜色.橙,颜色.红,颜色.橙,颜色.浅绿]        
        ]

def 堆栈是全色(堆栈):
    if len(堆栈) != 堆栈长度 :
        return False

    for i in 堆栈[1:] :
        if i!=堆栈[0]:
            return False
    return True

def 堆栈是同色(源堆栈):
    if len(源堆栈) == 1 :
        return True

    for i in 源堆栈[1:] :
        if i!=源堆栈[0]:
            return False
    return True

def 是否成功(堆栈列表):
    for 堆栈 in 堆栈列表:
        if len(堆栈)>0 and 堆栈是全色(堆栈)==False :
            return False
    return True

def 输出结果(堆栈):
    ## return 0
    print("matrix:")
    for i in 堆栈 :
        if len(i) == 0:
            print(i)
        else :
            ##print( [x.value for x in i])
            print( [x.name for x in i])
 

def 移动堆栈(堆栈列表,源位置,目标位置):   
    新堆栈=copy.deepcopy(堆栈列表);
    新堆栈[目标位置].append(新堆栈[源位置].pop())
    return 新堆栈
  
过程队列=[]
def 水塔_DFS(堆栈列表,级别):
    ##输出结果(堆栈列表)
    过程队列.append(堆栈列表)
    if 是否成功(堆栈列表) == True :
        print("成功！！！")
        return 0
   
    for 源位置 in range(len(堆栈列表)):
        if len(堆栈列表[源位置])==0 or 堆栈是全色(堆栈列表[源位置]) :
            continue

        已经有一次 = False
        for 目标位置 in range(len(堆栈列表)) :
            L=len(堆栈列表[目标位置])
            if 目标位置==源位置  or L==堆栈长度 or (L>0 and 堆栈列表[源位置][-1]!=堆栈列表[目标位置][-1]):
                continue

            if L==0 :               
                if 已经有一次 == True :
                    continue
                else :
                    已经有一次 = True

                if 堆栈是同色(堆栈列表[源位置]):
                    continue

            新列表 = 移动堆栈(堆栈列表,源位置,目标位置)
            if 过程队列.count(新列表)>0 :
                continue
            
            if 级别< 7 :
                print(级别,源位置,目标位置)
            if 水塔_DFS(新列表,级别+1) != 0 :
                continue
            else :
                return 0


    ##print("pop")
    过程队列.pop()
    return -1

def 数据检查(shuju):
    l=[b for 数组  in shuju for b in 数组]
    
    for i in l :
        x=l.count(i)
        if x!=4 :
            print(i,x)
            return False
        
    return True

if __name__ == "__main__":
     if 数据检查(开始列表) == False :
         print("数据检查失败！")
         exit(1)
        
     #输出结果(开始列表)
     if 水塔_DFS(开始列表,0)==0 :
         print("总共步数:",len(过程队列))
         for i in 过程队列 :
             输出结果(i)
     else :
         print("失败！")
