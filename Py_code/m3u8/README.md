# 下载视频用的小脚本

### 开发环境
python 3.4.1  
pip 9.0.3  
pycharm 2017.2.3

### 运行环境
windows 64位  
如果需要使用转码功能，则需要将转码器放在软件同级目录下  
转码器 [下载](https://gitee.com/dancer6666/m3u8/blob/master/d_m3u8/ffmpeg.exe)


### 基本功能
* 电影名称搜索下载
* 通过m3u8链接地址下载视频
* 分割下载
* 指定本地路径下载视频（一般用于继续上次未完成的任务）
* 直接下载ts文件并合并（一般用于下载视频中的某一段）
* 合并ts文件
* 解密本地ts文件
* ts转码MP4

### 温馨提示
由于平时工作和学习比较繁忙，这个项目现在已经被搁置很久了，  
不过好消息是我已经把这个项目做成了一个web服务，有兴趣可以访问 jubajie.com 精彩不要错过哦！