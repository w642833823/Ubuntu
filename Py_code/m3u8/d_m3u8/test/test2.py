#!/usr/bin/python3

import threading
from threading import Thread

import time

def th1():
    for i in range(6):
        time.sleep(1)
        print("线程1。。。"+str(i))

def th2():
    for i in range(3):
        time.sleep(1)
        print("线程2。。。"+str(i))


a = 0
lock = threading.Lock()

def cha(n):
    global a
    a = a+n
    a= a-n


def exit(n):

    lock.acquire()
    for i in range(1000):
        cha(n)
    lock.release()


if __name__=="__main__":

    n1 = threading.Thread(target=exit,args=(5,))
    n2 = threading.Thread(target=exit, args=(8,))
    n1.start()
    n2.start()
    n1.join()
    n2.join()
    print(a)


    # t1 = threading.Thread(target=th1)
    # t2 = threading.Thread(target=th2)
    #
    # t1.start()
    # t2.start()
    #
    # # t1.join()  # join 方法会使程序等线程执行完再继续向下执行
    # t2.join()

    print("----")