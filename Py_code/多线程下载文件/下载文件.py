#下载文件
from numpy import iterable
import requests
from tqdm import tqdm
import threading

#定义线程模块
class myThread (threading.Thread):
    def __init__(self,url):
        threading.Thread.__init__(self)
        self.url = url
    def run(self):
        download_file(self.url)
#定义下载模块
def download_file(url):
    resp = requests.get(url=url,stream=True)
    content_size = int(resp.headers['Content-Length'])/1024
    name = url.split('/')[-1]
    with open(name,"wb") as f:
        for data in tqdm(iterable=resp.iter_content(1024),
            total=content_size,unit='k',desc=name):
            f.write(data)
        print(name + "download finished!")
if __name__=='__main__':
    url_1 ='https://download.qt.io/official_releases/qt/6.3/6.3.0/submodules/qtwebsockets-everywhere-src-6.3.0.tar.xz'
    url_2 ='https://download.qt.io/official_releases/qt/6.2/6.2.4/single/qt-everywhere-src-6.2.4.tar.xz'
    #download_file(url_1)
    #download_file(url_2)
    thread1 = myThread(url_1)
    thread2 = myThread(url_2)
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print("下载完成！！！")

