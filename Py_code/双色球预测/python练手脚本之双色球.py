
import random
import argparse

# 双色球规则
red_ball_range = range(1, 34)
blue_ball_range = range(1, 17)
num_red_balls = 6
num_blue_balls = 1


def generate_lottery(red_nums, blue_nums):
    all_red = set(red_ball_range)
    all_blue = set(blue_ball_range)

    # 指定了部分红球和蓝球
    if red_nums and blue_nums:
        red_balls = set(red_nums)
        blue_balls = set(blue_nums)
    else:
        red_balls = set()
        blue_balls = set()

    # 随机生成剩余的红球和蓝球
    red_balls = red_balls.union(set(random.sample(all_red - red_balls, num_red_balls - len(red_balls))))
    blue_balls = blue_balls.union(set(random.sample(all_blue - blue_balls, num_blue_balls - len(blue_balls))))

    return sorted(red_balls), sorted(blue_balls)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='生成指定数量的双色球号码')
    parser.add_argument('--red-num', nargs='+', type=int, help='指定红球号码')
    parser.add_argument('--blue-num', nargs='+', type=int, help='指定蓝球号码')
    parser.add_argument('--num-groups', type=int, default=1, help='生成号码组数')
    args = parser.parse_args()

    for i in range(args.num_groups):
        red, blue = generate_lottery(args.red_num, args.blue_num)
        print(f'第 {i + 1} 组号码：')
        print('红球：', end='')
        for num in red:
            print(f'{num:02d}', end=' ')
        print('\n蓝球：', end='')
        for num in blue:
            print(f'{num:02d}', end=' ')
        print('\n')