from bs4 import BeautifulSoup as bs
import requests
import json

url='https://so.gushiwen.cn/gushi/tangshi.aspx'
header={
"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36"
}
fp=requests.get(url=url,headers=header)
soup=bs(fp.text,'lxml')
re=soup.select('.typecont span')
url_can=[]
for each in re:
    each_url='https://so.gushiwen.cn'+each.a['href']
    url_can.append(each_url)
def title(soup):
    return soup.select('.main3 .sons h1')[0].text
def author(soup):
    return soup.select('.main3 .sons .source')[0].find_all('a')[0].text
def acient(soup):
    return soup.select('.main3 .sons .source')[0].find_all('a')[1].text
def content(soup):
    return soup.select('.main3 .sons .contson')[0].text

result_list=[]
for one in url_can:
    one_result={}
    try:
        response=requests.get(url=one,headers=header)
    except:
        print('找不到该诗歌链接或网络有问题')
    else:
        one_soup=bs(response.text,'lxml')
        b=author(one_soup)
        a=title(one_soup)
        c=acient(one_soup)
        d=content(one_soup)
        one_result['title']=a
        one_result['author']=b
        one_result['time']=c
        one_result['content']=d.strip()
        print('%s 爬取完成!'%a)
        result_list.append(one_result)
print('over========================================')
with open('tangshi.json','w+',encoding='utf-8') as f:
    json.dump(result_list,fp=f,ensure_ascii=False)
