from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtCore import Qt

class CircleWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.circles = []

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        pen = QPen()
        pen.setWidth(2)
        painter.setPen(pen)

        # 绘制圆和连接线
        for i in range(len(self.circles)):
            circle = self.circles[i]
            center_x, center_y, radius = circle

            # 绘制圆
            painter.setBrush(Qt.NoBrush)
            painter.drawEllipse(center_x - radius, center_y - radius, radius * 2, radius * 2)

            # 获取上一个圆的中心点坐标
            if i > 0:
                prev_center_x, prev_center_y, _ = self.circles[i-1]
            else:
                prev_center_x, prev_center_y, _ = self.circles[-1]

            # 绘制连接线
            painter.drawLine(prev_center_x, prev_center_y, center_x, center_y)

    def addCircle(self, center_x, center_y, radius):
        self.circles.append((center_x, center_y, radius))
        self.update()

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Circle Painter')
        self.resize(800, 600)

        self.circle_widget = CircleWidget(self)
        self.setCentralWidget(self.circle_widget)

    def mousePressEvent(self, event):
        # 每次鼠标点击时添加一个新圆
        center_x = event.pos().x()
        center_y = event.pos().y()
        radius = 50
        self.circle_widget.addCircle(center_x, center_y, radius)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())