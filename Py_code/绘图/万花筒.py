import turtle  
turtle.setup(1000,700)
turtle.pensize(5)
color = ['orange','blue','yellow','black','green','tomato']  # 设置自己喜欢的几个颜色
for i in range(350):  # 让接下来的代码循环多少次，350次就有不错的效果了，要是前面加入初始化
    turtle.pencolor(color[i%6])   #  因为设置了六个颜色，要是想基于其他形状的，记得有多少条边设置多少个颜色，这里取余也要调整
    turtle.fd(i*1.15)  # 可以自己去搜搜递增类型的函数，让图形逐渐变大这里简单设置一个
    turtle.left(62)   # 我是基于正6编型画，每次左转60度，但是为了有交叉感，稍微增大几度
turtle.done()