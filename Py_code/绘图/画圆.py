import turtle as t


t.setup(1000,1000)#定义窗口尺寸
t.pensize(2.5)
for i in range(80): #定义循环40次
   t.speed(0.5)
   if i%2==0:
      t.pencolor('red')
   else:
      t.pencolor('blue')
   t.circle(200)
   t.fd(20)
   t.left(31)
t.done()