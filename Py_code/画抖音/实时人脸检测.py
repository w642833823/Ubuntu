import cv2
import cv2 as cv

if __name__ == '__main__':

    # 打开摄像头
    cap = cv2.VideoCapture(0)
    # 加载人脸检测器
    faca_detector = cv2.CascadeClassifier('./haarcascade_frontalface_alt.xml')

    # 读取每一帧图像
    while True:
        flag, frame = cap.read()  # flag是否读取了图片
        if not flag:
            break

        # 将图像转化为灰度图像
        gray = cv2.cvtColor(frame, code = cv2.COLOR_BGR2GRAY)
        # 对每一帧灰度图像进行人脸检测
        faces = faca_detector.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=10)
        # 对每一个检测到的人脸区域绘制检测方框
        for x,y,w,h in faces:
            cv2.rectangle(frame,
                          pt1 = (x,y),
                          pt2 = (x+w,y+h),
                          color=[0,0,255],
                          thickness=2)

        # 显示检测到的结果
        cv2.imshow('face', frame)
        # 设置显示时长
        key = cv2.waitKey(1000//24)  # 注意要用整除//，因为毫秒为整数
        # 按q键退出
        if key == ord('q'):
            break

    # 销毁内存
    cv2.destroyAllWindows()
    cap.release()
