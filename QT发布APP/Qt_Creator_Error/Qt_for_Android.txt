用 sudo gedit /etc/profile打开文件，配置环境变量 
#设置Qt的环境变量
#以下内容可以存放到以root下使用的~/.bashrc中
#或者存放的不要root权限的/etc/profile中

# set PATH to QT
export QTDIR=/opt/Qt5.14.2/Tools/QtCreator
export PATH=$QTDIR/bin:$PATH
export MANPATH=$QTDIR/doc/man:$MANPATH
export LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH

#这句不可少
# set PATH to QT6.3.2
export PATH="/opt/Qt/Tools/QtCreator/bin:$PATH"
export PATH="/opt/Qt/6.3.2/gcc_64/bin:$PATH"

#set PATH to JDK_HOME
export JAVA_HOME=/home/w642833823/Android/jdk1.8.0_92
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH




#qt for android环境搭建


#set PATH to NDK_HOME
NDK_HOME=/home/w642833823/Android/android-ndk-r20
export PATH=$PATH:$NDK_HOME



#set PATH to ANT_HOME
    export ANT_HOME=/home/w642833823/Android/apache-ant-1.9.7
    export PATH=$PATH:${ANT_HOME}/bin

