问题描述：在Ubuntu下，点击Qt图标后，Qt卡死，整体布局不显示

解决方法：删除Qt安装系统配置目录下的QtProject文件夹及文件夹中的文件

1、查找当前系统的QtProject文件夹所在的目录

find / -name QtProject

2、命令行会输出QtProject文件夹所在的目录

3、删除QtProject文件夹

rm -fr /home/embedfire/.config/QtProject
4、重新打开Qt软件即可正常显示
