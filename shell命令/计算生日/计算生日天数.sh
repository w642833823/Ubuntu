#!/bin/bash

# 提示用户输入生日，‌格式为 YYYY/MM/DD
read -p "请输入你的生日(2012/12/05 ):" birthday

# 使用 date 命令从用户输入的生日字符串中提取月份
m=$(date --date="$birthday" +%m)
echo "提取月份：$m"
# 使用 date 命令从用户输入的生日字符串中提取日期
d=$(date --date="$birthday" +%d)
echo "提取日期：$d"
# 使用 date 命令获取当前年份
y=$(date +%Y)
echo "提取年份：$y"
# 使用 date 命令将提取的年、‌月、‌日组合成完整的生日日期，‌并转换为时间戳
birth=$(date --date="$y$m$d" +%s)
echo "总时间辍：$birth"
# 使用 date 命令获取当前时间的时间戳
now=$(date +%s)
echo "当前时间的时间戳：$now"
# 计算生日时间戳与当前时间戳的差值
diff=$((birth - now))
echo "时间差：$diff"

# 如果差值为负数，‌说明生日已经过去，‌需要将生日年份加1，‌并重新计算时间戳差值
if [ "$diff" -lt "0" ]; then
    birth=$(date --date="$(($y+1))$m$d" +%s)
    diff=$((birth - now))
    echo "生日已经过去，‌需要将生日年份加1，‌并重新计算时间戳差值：$diff"

fi

# 计算差值对应的天数
days=$((diff / 86400))

# 计算差值对应的小时数（‌取模后除以3600）‌
hours=$(( (diff % 86400) / 3600 ))

# 打印结果，‌显示距离生日还有多少天和小时
echo "距离你的生日还有: $days 天 $hours 小时。‌‌"

