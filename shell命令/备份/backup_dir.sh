#!/bin/bash

SOURCE_DIR=$1
BACKUP_DIR=$2

if [ ! -d "$SOURCE_DIR" ]; then
  echo "指定的源目录不存在."
  exit 1
fi

TIMESTAMP=$(date +"%Y%m%d%H%M%S")
BACKUP_FILE="$BACKUP_DIR/backup_$TIMESTAMP.tar.gz"

tar -czf "$BACKUP_FILE" -C "$SOURCE_DIR" .

echo "备份已在创建 $BACKUP_FILE."
