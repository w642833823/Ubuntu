#!/bin/bash
# 检查是否有参数传入
if [ $# -eq 0 ]; then
    echo "用法: $0 参数1 参数2 ..."
    exit 1
fi

# 遍历所有传入的参数
for arg in "$@"; do
    echo "接收到的参数: $arg"
done

# 或者，你可以单独访问它们
#echo "第一个参数: $1"
#echo "第二个参数: $2"
LOCK_FILE=$1


#!/bin/bash

FILE="$LOCK_FILE"

# 使用chattr添加i属性来'加锁'文件
sudo chattr +i "$FILE"
echo "文件已'加锁'（添加了i属性）。"
