#!/bin/bash


# 检查是否有参数传入
if [ $# -eq 0 ]; then
    echo "用法: $0 参数1 参数2 ..."
    exit 1
fi

# 遍历所有传入的参数
for arg in "$@"; do
    echo "接收到的参数: $arg"
done

# 或者，你可以单独访问它们
#echo "第一个参数: $1"
#echo "第二个参数: $2"

LOCK_FILE="$1"

echo "正在使用lsattr DIR 查看文件是否加锁"
FILE="$LOCK_FILE"

# 使用lsattr检查文件是否有i属性
if lsattr "$LOCK_FILE" | grep -q '^....i'; then
    echo "当前$LOCK_FILE文件已被'加锁'（具有i属性）。"
else
    echo "当前$LOCK_FILE文件未被'加锁'。"
fi
