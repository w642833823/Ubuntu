#!/bin/bash

# 定义前景色输出函数
black_text(){
    echo -e "\033[30m $1 \033[0m"
}

red_text(){
    echo -e "\033[31m $1 \033[0m"
}

green_text(){
    echo -e "\033[32m $1 \033[0m"
}

yellow_text(){
    echo -e "\033[33m $1 \033[0m"
}

blue_text(){
    echo -e "\033[34m $1 \033[0m"
}

purple_text(){
    echo -e "\033[35m $1 \033[0m"
}

cyan_text(){
    echo -e "\033[36m $1 \033[0m"
}

white_text(){
    echo -e "\033[37m $1 \033[0m"
}

# 定义背景色输出函数
black_bg(){
    echo -e "\033[40m $1 \033[0m"
}

red_bg(){
    echo -e "\033[41m $1 \033[0m"
}

green_bg(){
    echo -e "\033[42m $1 \033[0m"
}

yellow_bg(){
    echo -e "\033[43m $1 \033[0m"
}

blue_bg(){
    echo -e "\033[44m $1 \033[0m"
}

purple_bg(){
    echo -e "\033[45m $1 \033[0m"
}

cyan_bg(){
    echo -e "\033[46m $1 \033[0m"
}

white_bg(){
    echo -e "\033[47m $1 \033[0m"
}

# 显示前景色
echo "前景色示例："
black_text "这是黑色的文本"
red_text "这是红色的文本"
green_text "这是绿色的文本"
yellow_text "这是黄色的文本"
blue_text "这是蓝色的文本"
purple_text "这是紫色的文本"
cyan_text "这是青色的文本"
white_text "这是白色的文本"

# 显示背景色
echo "背景色示例："
black_bg "这是黑色背景的文本"
red_bg "这是红色背景的文本"
green_bg "这是绿色背景的文本"
yellow_bg "这是黄色背景的文本"
blue_bg "这是蓝色背景的文本"
purple_bg "这是紫色背景的文本"
cyan_bg "这是青色背景的文本"
white_bg "这是白色背景的文本"

