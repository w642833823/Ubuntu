#!/bin/bash
# w6428823:           gudujian
# Created Time:       2024-10-20
#!/bin/bash

# 指定要查找的文件或文件夹名称
TARGET="$1"

# 使用find命令递归查找当前目录及其子目录下的指定文件或文件夹
find . -type f -name "$TARGET" -o -type d -name "$TARGET" > temp_files.txt

# 检查是否找到匹配的文件或文件夹
if [ -s temp_files.txt ]; then
    echo "找到以下匹配的文件或文件夹："
    cat temp_files.txt
    echo "请选择要删除的文件或文件夹的编号（输入行号，多个编号用空格分隔）："
    read -a choices

    for choice in "${choices[@]}"; do
        # 根据用户选择的行号获取文件或文件夹路径
        file_to_delete=$(sed -n "${choice}p" temp_files.txt)
        echo "删除 $file_to_delete"
        rm -rf "$file_to_delete"
    done

    echo "删除完成。"
else
    echo "未找到匹配的文件或文件夹。"
fi

# 删除临时文件
rm temp_files.txt

