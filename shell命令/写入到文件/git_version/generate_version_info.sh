#!/bin/bash
#当前文件必须在git目录下执行
# 获取Git分支名
BRANCH=$(git rev-parse --abbrev-ref HEAD)

# 获取Git提交哈希
COMMIT=$(git rev-parse HEAD)

# 获取Git提交时间
DATE=$(git show -s --format=%ci $COMMIT)

# 将版本信息写入头文件
cat > version_info.h << EOF
#ifndef VERSION_INFO_H
#define VERSION_INFO_H

#define GIT_BRANCH "$BRANCH"
#define GIT_COMMIT "$COMMIT"
#define GIT_DATE "$DATE"

#endif // VERSION_INFO_H
EOF

