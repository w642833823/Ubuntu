#include <stdio.h>
#include "version_info.h"

int main() {
    printf("Git Branch: %s\n", GIT_BRANCH);
    printf("Git Commit: %s\n", GIT_COMMIT);
    printf("Git Date: %s\n", GIT_DATE);

    // 程序的其余部分...

    return 0;
}

