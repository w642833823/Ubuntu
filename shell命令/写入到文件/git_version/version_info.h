#ifndef VERSION_INFO_H
#define VERSION_INFO_H

#define GIT_BRANCH "master"
#define GIT_COMMIT "069efc6be48da6839684a0c013f30ee65f06db9a"
#define GIT_DATE "2024-08-14 21:44:58 +0800"

#endif // VERSION_INFO_H
