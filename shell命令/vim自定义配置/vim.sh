#！/bin/bash
echo"vim自定义配置使用方法将下面内容写入 .vimrc中"
echo "请使用root权限写入"

cat >.vimrc <<EOF
" 语法高亮
syntax on
" 显示行号
set number
" 自动缩进
set autoindent
" 鼠标移动光标
set mouse=a
" 自动插入文件头
autocmd BufNewFile *.py,*.sh call SetComment()
" 自动定位到文件末尾
autocmd BufNewFile * normal G

function! SetComment()
    if expand("%:e") ==# 'sh'
        call setline(1, '#!/bin/bash')
        call setline(2, '# w6428823:           gudujian')
        call setline(3, '# Created Time:       ' . strftime("%Y-%m-%d"))
    endif
    if expand("%:e") ==# 'py'
        call setline(1, '#!/usr/bin/env python')
        call setline(2, '# -*- coding: utf-8 -*-')
        call setline(3, '# w6428823:           gudujian')
        call setline(4, '# Created Time:       ' . strftime("%Y-%m-%d"))
    endif
endfunction
EOF

