## 使用如下
## python3 彩票.py 2 12
import random
import sys
# 获取参数信息
r_num = int(sys.argv[1])
b_num = int(sys.argv[2])
# 确定参数范围是否合理
if (r_num>0 and r_num<7 and b_num>0 and b_num<15):
# 产生随机数
    x=random.sample(range(1,16),r_num)
    # 排序默认升序从小大大
    x.sort()
    y=random.sample(range(1,33),b_num)
    y.sort()
    for red in x:
        print("\033[34m %s" %red ,end=' ')
    print('+ ',end=' ')
    for blue in y:
        print("\033[31m %s" %blue,end=' ')
    print ("\033[0m 选号完毕！愿君高中！",end=' \n')
else:
     print('输入有误，第一个数应大于0小于7，第二个数应大于6小于17')
