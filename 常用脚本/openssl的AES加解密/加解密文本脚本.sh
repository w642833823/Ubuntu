#!/bin/bash

# 生成待加密文本
echo "123456789012345" > en_in.txt

# 密匙: 32位十六进制
key=3132333431323334313233343132333431323334313233343132333431323334
# 加密向量: 16位十六进制
iv=31323334313233343132333431323334

# 源文件内容
echo source: 
cat en_in.txt

# 加密
openssl enc -e -aes-256-cbc -nopad -K $key -iv ${iv} -in en_in.txt -out en_out.txt

# 加密后内容
echo enc out: 
cat en_out.txt | hexdump -C

# 解密
openssl enc -d -aes-256-cbc -nopad -K $key -iv ${iv} -in en_out.txt -out de_out.txt

# 解密后内容
echo dec out: 
cat de_out.txt