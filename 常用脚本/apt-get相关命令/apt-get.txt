apt-get相关命令

apt-cache search package   #搜索包
apt-cache show package #获取包的相关信息，如说明、大小、版本等
apt-get install package #安装包
apt-get install package --reinstall #重新安装包
apt-get -f install #强制安装
apt-get remove package #删除包
apt-get remove package - - purge #删除包，包括删除配置文件等
apt-get autoremove #自动删除不需要的包
apt-get update #更新源
apt-get upgrade #更新已安装的包
apt-get dist-upgrade #升级系统
apt-get dselect-upgrade #使用 dselect 升级
apt-cache depends package #了解使用依赖
apt-cache rdepends package #了解某个具体的依赖
apt-get build-dep package #安装相关的编译环境
apt-get source package #下载该包的源代码
apt-get clean && sudo apt-get autoclean #清理下载文件的存档
apt-get check #检查是否有损坏的依赖
