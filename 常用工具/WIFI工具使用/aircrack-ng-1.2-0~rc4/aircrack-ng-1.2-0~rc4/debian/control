Source: aircrack-ng
Section: net
Priority: optional
Maintainer: Debian Security Tools Packaging Team <pkg-security-team@lists.alioth.debian.org>
Uploaders: Carlos Alberto Lopez Perez <clopez@igalia.com>,
           Samuel Henrique <samueloph@gmail.com>
Homepage: http://www.aircrack-ng.org/
Build-Depends:
 debhelper (>= 10),
 dh-python,
 libgcrypt-dev | libgcrypt20-dev | libgcrypt11-dev,
 libnl-genl-3-dev [linux-any],
 libpcap0.8-dev,
 libpcre3-dev,
 libsqlite3-dev,
 pkg-config,
 python-all,
 python-setuptools,
 zlib1g-dev
Standards-Version: 4.1.1
Vcs-Browser: https://anonscm.debian.org/git/pkg-security/aircrack-ng.git
Vcs-Git: https://anonscm.debian.org/git/pkg-security/aircrack-ng.git

Package: aircrack-ng
Architecture: any
Depends:
 iw [linux-any],
 wireless-tools [linux-any],
 ethtool,
 usbutils,
 rfkill,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ieee-data
Description: wireless WEP/WPA cracking utilities
 aircrack-ng is an 802.11a/b/g WEP/WPA cracking program that can recover a
 40-bit, 104-bit, 256-bit or 512-bit WEP key once enough encrypted packets
 have been gathered. Also it can attack WPA1/2 networks with some advanced
 methods or simply by brute force.
 .
 It implements the standard FMS attack along with some optimizations,
 thus making the attack much faster compared to other WEP cracking tools.
 It can also fully use a multiprocessor system to its full power in order
 to speed up the cracking process.
 .
 aircrack-ng is a fork of aircrack, as that project has been stopped by
 the upstream maintainer.

Package: airgraph-ng
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, graphviz
Recommends: aircrack-ng
Description: Tool to graph txt files created by aircrack-ng
 airgraph-ng is a tool to create a graph ouf of the txt file created by airodump
 with its -w option. The graph shows the relationships between the clients and
 the access points.
