####!/bin/bash   ##指定解释器：由哪个程序来执行脚本内容；#!表示幻数

###注意:#!/bin/bash必须写在第一行,否则会被认为是注释
#!/bin/sh
#sudo chmod +x Aircrack-ng.sh
#sudo chmod 777 ./Aircrack-ng.sh
#sudo sudo ./Aircrack-ng.sh
echo -e "\n\033[1;35m 开始安装Ettercap  \n \033[0m\n"

sudo apt install ettercap-text-only
sudo apt install ettercap-graphical

echo "\n"
echo -e "\033[1;42m apt-get update  检查已安装软件包的可用更新......\033[0m \n"
apt-get update
echo "\n"

