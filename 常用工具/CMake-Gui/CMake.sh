####!/bin/bash   ##指定解释器：由哪个程序来执行脚本内容；#!表示幻数

###注意:#!/bin/bash必须写在第一行,否则会被认为是注释
#!/bin/sh
#sudo chmod +x gdb.sh
#sudo chmod 777 ./gdb.sh
#sudo sudo ./gdb.sh
echo "\n"
echo -e "\n\033[1m;32m安装需要的依赖 \n \033[0m\n"

sudo apt install git cmake cmake-gui g++ qtbase5-dev uwsgi clearsilver-dev libgrantlee5-dev uuid-dev libcap-dev libzmq3-dev libpwquality-dev libmemcached-dev libjemalloc-dev
