####!/bin/bash   ##指定解释器：由哪个程序来执行脚本内容；#!表示幻数

###注意:#!/bin/bash必须写在第一行,否则会被认为是注释
#!/bin/sh
#sudo chmod +x update_QT.sh
#sudo chmod 777 ./update_QT.sh
#sudo sudo ./update_QT.sh
echo "\n"
echo -e "\n\033[1;35m安装OpenCV \n \033[0m\n"
apt install python3-pip
pip3 install numpy
pip3 install opencv-python
apt install python3-opencv
echo -e "\033[1;33m apt-get update  检查已安装软件包的可用更新......\033[0m \n"
apt-get update
echo "\n"
