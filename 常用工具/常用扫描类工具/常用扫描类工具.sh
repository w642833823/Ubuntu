####!/bin/bash   ##指定解释器：由哪个程序来执行脚本内容；#!表示幻数

###注意:#!/bin/bash必须写在第一行,否则会被认为是注释
#!/bin/sh
#sudo chmod +x 常用扫描类工具.sh
#sudo chmod 777 ./常用扫描类工具.sh
#sudo sudo ./常用扫描类工具.sh
echo "\n"

echo -e "\n\033[1;35m 安装 Nmap扫描工具 \n \033[0m\n"
sudo apt update
sudo apt-get install nmap
echo "\n"
echo -e "\n\033[1;35m 安装 Zmap快速扫描工具 \n \033[0m\n"
echo -e "\n用法：
zmap -B 10M -p 80 -n 100000 -o results.txt
-B 是指的是带宽限制 -p 端口 -n 扫描多少个目标 -o 输出结果\n"

sudo apt update
sudo apt-get install zmap
echo "\n"

